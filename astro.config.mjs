import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";

import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  site: "https://pgrasland.gitlab.io/decapcms-astro",
  base: "/decapcms-astro",
  prefetch: true,
  integrations: [mdx(), sitemap()],
  outDir: "public",
  publicDir: "static",
});
